﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JeTest.RestClient.Models;
using System.Net.Http;
using System.Linq;
using JeTest.RestClient.Exceptions;
using JeTest.RestClient.Tests.AuthProviders;

namespace JeTest.RestClient.Tests
{
    [TestClass]
    public class DefaultRequestTests
    {
        [TestMethod]
        public void Constructor_AssertHttpRequestMessageSetup()
        {
            var request = new DefaultRequest("http://localhost", "restaurants", HttpMethod.Get);

            Assert.AreEqual("http://localhost/restaurants", request.HttpRequestMessage.RequestUri.AbsoluteUri);
            Assert.AreEqual(HttpMethod.Get, request.HttpRequestMessage.Method);
        }

        [TestMethod]
        public void AddHeader_CustomHeader_AssertHeaderIsSet()
        {
            var request = new DefaultRequest("http://localhost", "restaurants", HttpMethod.Get);

            request.AddHeader("Accept-Tenant", "uk");

            var header = request.HttpRequestMessage.Headers.GetValues("Accept-Tenant").FirstOrDefault();
            Assert.AreEqual("uk", header);
        }

        [TestMethod]
        public void AddHeader_ContentType_AssertAcceptHeaderIsSet()
        {
            var request = new DefaultRequest("http://localhost", "restaurants", HttpMethod.Get);

            request.AddHeader("Content-Type", "application/json");
            Assert.AreEqual("application/json", request.HttpRequestMessage.Headers.GetValues("Accept").FirstOrDefault());
        }

        [TestMethod]
        public void AddUrlParameter_QueryParameter_AssertUrlIsCorrect()
        {
            var request = new DefaultRequest("http://localhost", "restaurants", HttpMethod.Get);

            request.AddUrlParameter("q", "w25na");

            Assert.AreEqual("http://localhost/restaurants?q=w25na", request.HttpRequestMessage.RequestUri.AbsoluteUri);
        }

        [TestMethod]
        public void AddUrlParameter_MoreQueryParameters_AssertUrlIsCorrect()
        {
            var request = new DefaultRequest("http://localhost", "restaurants", HttpMethod.Get);

            request.AddUrlParameter("q", "w25na");
            request.AddUrlParameter("name", "PizzaExpress");

            Assert.AreEqual("http://localhost/restaurants?q=w25na&name=PizzaExpress", request.HttpRequestMessage.RequestUri.AbsoluteUri);
        }
        
        [TestMethod]
        public void AddHeader_AuthorizationHeader_AssertAuthorizationtHeaderIsSet()
        {
            var request = new DefaultRequest("http://localhost", "restaurants", HttpMethod.Get);
            request.AddHeader("Authorization", "Basic VGVjaFRlc3RBUEk6dXNlcjI=");

            Assert.AreEqual("Basic VGVjaFRlc3RBUEk6dXNlcjI=", request.HttpRequestMessage.Headers.GetValues("Authorization").FirstOrDefault());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidHeaderException))]
        public void AddHeader_IncorrectAuthorizationHeader_ExpectInvalidHeader()
        {
            var request = new DefaultRequest("http://localhost", "restaurants", HttpMethod.Get);
            request.AddHeader("Authorization", "GVjaFRlc3RBUEk6dXNlcjI=");
        }

        [TestMethod]
        public void AddAuthorizationHeader_UsingAuthProvider_AssertAuthorizationHeaderIsSet()
        {
            var request = new DefaultRequest("http://localhost", "restaurants", HttpMethod.Get);
            request.AddAuthorizationHeader(new BasicAuthToken("Basic VGVjaFRlc3RBUEk6dXNlcjI="));

            Assert.AreEqual("Basic VGVjaFRlc3RBUEk6dXNlcjI=", request.HttpRequestMessage.Headers.GetValues("Authorization").FirstOrDefault());
        }
        
    }
}
