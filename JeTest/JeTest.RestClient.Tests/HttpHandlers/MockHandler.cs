﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace JeTest.RestClient.Tests.HttpHandlers
{
    public class MockHandler : HttpMessageHandler
    {
        private HttpResponseMessage _response;
        private HttpRequestException _genericHttpException;

        public MockHandler(HttpResponseMessage response, HttpRequestException e = null)
        {
            _response = response;
            _genericHttpException = e;
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var responseTask = new TaskCompletionSource<HttpResponseMessage>();
            responseTask.SetResult(_response);

            if (_genericHttpException != null)
            {
                throw _genericHttpException;
            }

            return responseTask.Task;
        }
    }
}
