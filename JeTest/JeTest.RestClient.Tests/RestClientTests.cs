﻿using JeTest.RestClient.Serializers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Net.Http;
using JeTest.RestClient.Exceptions;
using System.Threading.Tasks;
using Newtonsoft.Json;
using JeTest.RestClient.Tests.HttpHandlers;
using System.Linq;
using JeTest.RestClient.Models;

namespace JeTest.RestClient.Tests
{
    [TestClass]
    public class RestClientTests
    {
        private static Mock<ISerializer> _mockJsonSerializer;
        private static Mock<IRequestFactory> _mockRequestFactory;
        private static Mock<IRequest> _mockRequest;
        private static HttpClient _httpClient;
        private static RestClient _restClient;

        [ClassInitialize]
        public static void Setup(TestContext tc)
        {
            _mockJsonSerializer = new Mock<ISerializer>();
            _mockRequestFactory = new Mock<IRequestFactory>();
            _mockRequest = new Mock<IRequest>();

            var mockedRequest = new DefaultRequest("http://localhost", "resource", HttpMethod.Get);
            _mockRequestFactory.SetupAllProperties();
            _mockRequestFactory.Setup(m => m.GetInstance(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<HttpMethod>())).Returns(mockedRequest);
            
            _mockJsonSerializer.Setup(m => m.Deserialize<object>(It.IsAny<string>())).Returns(new object());
            _httpClient = new HttpClient();
            _restClient = new RestClient(_httpClient, _mockJsonSerializer.Object);
        }

       
        [TestMethod]
        public void AddDefaultHeader_CustomHeader_AssertHeaderIsAddedToHttpClient()
        {
            _restClient.AddDefaultHeader("Accept-Tenant", "uk");
            Assert.AreEqual("uk", _httpClient.DefaultRequestHeaders.GetValues("Accept-Tenant").FirstOrDefault());
        }

        [TestMethod]
        public void AddDefaultHeader_ContentType_AssertAcceptHeaderIsSet()
        {
            _restClient.AddDefaultHeader("Content-Type", "application/json");
            Assert.AreEqual("application/json", _httpClient.DefaultRequestHeaders.GetValues("Accept").FirstOrDefault());
        }

        [TestMethod]
        public void AddDefaultHeader_AuthorizationHeader_AssertAuthorizationtHeaderIsSet()
        {
            _restClient.AddDefaultHeader("Authorization", "Basic VGVjaFRlc3RBUEk6dXNlcjI=");
            Assert.AreEqual("Basic VGVjaFRlc3RBUEk6dXNlcjI=", _httpClient.DefaultRequestHeaders.GetValues("Authorization").FirstOrDefault());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidHeaderException))]
        public void AddDefaultHeader_IncorrectAuthorizationHeader_ExpectInvalidHeader()
        {
            _restClient.AddDefaultHeader("Authorization", "GVjaFRlc3RBUEk6dXNlcjI=");
        }

        [TestMethod]
        public async Task Execute_OKResponseWithEmptyData_VerifySerializerIsCalled()
        {
            var expectedObject = new object();

            var fakeResponseMessage = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.OK,
                Content = new StringContent(JsonConvert.SerializeObject(expectedObject))
            };

            var fakeMessageHandler = new MockHandler(fakeResponseMessage);
            var mockedHttpClient =  new HttpClient(fakeMessageHandler);

            _mockRequest.SetupGet<HttpRequestMessage>(m => m.HttpRequestMessage).Returns(new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new System.Uri("http://localhost")
            });

            _mockRequestFactory.Setup(m => m.GetInstance(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<HttpMethod>())).Returns(_mockRequest.Object);

            _restClient = new RestClient(mockedHttpClient, _mockJsonSerializer.Object);

            var result = await _restClient.Execute<object>(_mockRequestFactory.Object.GetInstance("http://localhost", "restaurants", HttpMethod.Get)).ConfigureAwait(false);

            _mockJsonSerializer.Verify(m => m.Deserialize<object>(It.IsAny<string>()));
        }

        [TestMethod]
        [ExpectedException(typeof(ClientException))]
        public async Task Execute_400RangeResponse_ExpectClientException()
        {
            var expectedObject = new object();

            var fakeResponseMessage = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.BadRequest,
                Content = new StringContent(JsonConvert.SerializeObject(expectedObject))
            };

            var fakeMessageHandler = new MockHandler(fakeResponseMessage);
            var mockedHttpClient = new HttpClient(fakeMessageHandler);

            _mockRequest.SetupGet<HttpRequestMessage>(m => m.HttpRequestMessage).Returns(new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new System.Uri("http://localhost")
            });
            
            _mockRequestFactory.Setup(m => m.GetInstance(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<HttpMethod>())).Returns(_mockRequest.Object);

            _restClient = new RestClient(mockedHttpClient, _mockJsonSerializer.Object);

            var result = await _restClient.Execute<object>(_mockRequestFactory.Object.GetInstance("http://localhost", "restaurants", HttpMethod.Get)).ConfigureAwait(false);
        }

        [TestMethod]
        [ExpectedException(typeof(ServerException))]
        public async Task Execute_500RangeResponse_ExpectServerException()
        {
            var expectedObject = new object();

            var fakeResponseMessage = new HttpResponseMessage
            {
                StatusCode = System.Net.HttpStatusCode.InternalServerError,
                Content = new StringContent(JsonConvert.SerializeObject(expectedObject))
            };

            var fakeMessageHandler = new MockHandler(fakeResponseMessage);
            var mockedHttpClient = new HttpClient(fakeMessageHandler);

            _mockRequest.SetupGet<HttpRequestMessage>(m => m.HttpRequestMessage).Returns(new HttpRequestMessage {
                 Method = HttpMethod.Get,
                 RequestUri = new System.Uri("http://localhost")
            });

            _mockRequestFactory.Setup(m => m.GetInstance(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<HttpMethod>())).Returns(_mockRequest.Object);

            _restClient = new RestClient(mockedHttpClient, _mockJsonSerializer.Object);
            var result = await _restClient.Execute<object>(_mockRequestFactory.Object.GetInstance("http://localhost", "restaurants", HttpMethod.Get)).ConfigureAwait(false);
        }
    }
}
