﻿using JeTest.RestClient.AuthProvider;

namespace JeTest.RestClient.Tests.AuthProviders
{
    public class BasicAuthToken : IAuthProvider
    {
        string _token;

        public BasicAuthToken(string token) {
            _token = token;
        }
        
        public string Token
        {
            get
            {
                return _token;
            }
        }
    }
}
