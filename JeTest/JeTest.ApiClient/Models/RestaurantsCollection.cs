﻿using System.Collections.Generic;

namespace JeTest.ApiClient.Models
{
    public class RestaurantsCollection
    {
        public List<Restaurant> Restaurants;
    }
}
