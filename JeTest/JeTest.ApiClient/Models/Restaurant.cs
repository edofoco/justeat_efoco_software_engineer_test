﻿using Newtonsoft.Json;

namespace JeTest.ApiClient.Models
{
    public class Restaurant
    {
        public string Name;
        [JsonProperty(PropertyName = "RatingAverage")]
        public double Rating;
        public bool IsOpenNow;
        [JsonProperty(PropertyName = "CuisineTypes")]
        public Cuisine[] CuisineTypes { get; set; }
    }
}
