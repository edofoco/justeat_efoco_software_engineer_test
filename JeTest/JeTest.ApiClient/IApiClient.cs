﻿using JeTest.ApiClient.Models;
using System.Threading.Tasks;

namespace JeTest.ApiClient
{
    public interface IApiClient
    {
        Task<RestaurantsCollection> GetRestaurants(string postcode);
    }
}
