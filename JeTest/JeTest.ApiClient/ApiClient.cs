﻿using System.Threading.Tasks;
using JeTest.ApiClient.Models;
using JeTest.RestClient;
using System.Net.Http;
using JeTest.RestClient.AuthProvider;

namespace JeTest.ApiClient
{
    public class ApiClient : IApiClient
    {
        private IRestClient _restClient;
        private IAuthProvider _authProvider;
        private IRequestFactory _requestFactory;
        private string _baseUri;

        public ApiClient(string baseUri, string tenantHeader, string hostHeader, string languageHeader, IRestClient restClient, IRequestFactory requestFactory, IAuthProvider authProvider) {

            _restClient = restClient;
            _requestFactory = requestFactory;
            _authProvider = authProvider;
            _baseUri = baseUri;

            _restClient.AddDefaultHeader("Accept-Tenant", tenantHeader);
            _restClient.AddDefaultHeader("Host", hostHeader);
            _restClient.AddDefaultHeader("Accept-Language", languageHeader);
        }
        
        public async Task<RestaurantsCollection> GetRestaurants(string postcode)
        {
            var request = _requestFactory.GetInstance(_baseUri, "restaurants", HttpMethod.Get);
            request.AddUrlParameter("q", postcode);
            request.AddAuthorizationHeader(_authProvider);

            return await _restClient.Execute<RestaurantsCollection>(request).ConfigureAwait(false);
        }
    }
}
