# Answers to technical questions
##### 1.  How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add. `

I spent 4 hours total (+ sometime editing the markdown). If I had more time I would improve the following areas:
>  **RestClient.cs** - The CreateRequest method which at the moment returns a  concrete implementation of the DefaultRequest model. I would completely remove this method and instead create a RequestFactory implementation to inject directly into the IApiClient.cs. I do not think the RestClient.cs should have the responsibility of creating the IRequest. The dilemma was whether to create a RequestBuilder or to use a factory, and because of this indecision I left it as it was - I ran out of time and didn't change it, this is my biggest regret.

> **Program.cs** - Because of time constraints I opted to read the user input using Console.Readline() instead of parsing command options.

> **RestaurantResultsTableOutputter.cs** - Only has 1 unit test. It also only supports restaurants with names up to 50 characters. Because of time constraints I didn't focus too much on having a nice output system. The interface allows to create other outputters though.

>   **JsonSerializer** - Has 0 unit tests.

>  **Logging** - I would have used Log4Net. 


##### 2. What was the most useful feature that was added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.`
- **String Interpolation**  
`$"{baseUrl}/{resource}"`


- **Null Conditional Operator**                  
```csharp
if (aspectRatio?.value != null)
{
    var newAspectRatioMetadata = new DamServiceExtension.iMetaPropertyBean
    {
    	dpath = FilterTypes.CustomAspectRatio,
        value = aspectRatio.value
    };

	damInstance.metadata[i] = newAspectRatioMetadata;
}
```

##### 3. How would you track down a performance issue in production? Have you ever had to do this?
There can be various performance issues ranging from CPU issues, connectivity issues, software issues and so on. In the company I work for we use Nagios as a monitoring tool while logging all the sotware logs on CloudWatch. We are currently looking into and testing Splunk using CloudWatch as our data source.


##### 4. How would you improve the JUST EAT APIs that you just used?
- Pagination
- Cache Headers - knowing opening and closing times of restaurants allows to set max-age headers
- 415 Unsopported Media Type bug - occurs when GETting /restaurants without query parameters


##### 5. Please describe yourself using JSON.
```
{  
   "name":"Edoardo",
   "lastname":"Foco",
   "nationality":"Italian",
   "profession":"Software Engineer",
   "degree":"BEng Software Engineering",
   "birth_date":"22/12/1990",
   "interests":[  
      "kitesurfing",
      "surfing",
      "snowboarding",
      "programming",
      "travelling",
      "beers"
   ],
   "languages":{  
      "spoken":[  
         "Italian",
         "English"
      ],
      "written":[  
         "Italian",
         "English",
         "C#",
         "PHP",
         "JS"
      ]
   }
}
```