﻿using JeTest.ApiClient.Models;
using System.Collections.Generic;

namespace JeTest
{
    public interface IRestaurantsResultsParser
    {
        string ParseResults(List<Restaurant> restaurants);
    }
}
