﻿using System;
using StructureMap;
using System.Net.Http;
using JeTest.RestClient.Serializers;
using JeTest.RestClient;
using JeTest.ApiClient;
using System.Configuration;
using JeTest;
using JeTest.RestClient.AuthProvider;

namespace JustEatTest.IoC
{
    class CoreRegistry : Registry
    {
        public CoreRegistry()
        {
            For<ISerializer>().Use<JsonSerializer>();

            For<IRequestFactory>().Use<DefaultRequestFactory>();

            For<IRestClient>().Use<RestClient>()
                .Ctor<HttpClient>().Is(new HttpClient());
             
            For<IRestaurantsResultsParser>().Use<RestaurantsResultsTableParser>();

            For<IAuthProvider>().Use<SimpleAuthTokenProvider>().Ctor<string>().Is(ConfigurationManager.AppSettings["JustEat.HttpHeader.Authorization"]);

            For<IApiClient>().Use<ApiClient>()
                .Ctor<string>("tenantHeader").Is(ConfigurationManager.AppSettings["JustEat.HttpHeader.Tenant"])
                .Ctor<string>("languageHeader").Is(ConfigurationManager.AppSettings["JustEat.HttpHeader.Language"])
                .Ctor<string>("hostHeader").Is(ConfigurationManager.AppSettings["JustEat.HttpHeader.Host"])
                .Ctor<string>("baseUri").Is(ConfigurationManager.AppSettings["JustEat.BaseUri"]);
        }

    }
}
