﻿using JeTest.ApiClient;
using JeTest.RestClient.Exceptions;
using JustEatTest.IoC;
using StructureMap;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JeTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var ioc = new Container(new CoreRegistry());
            var apiClient = ioc.GetInstance<IApiClient>();
            var resultsParser = ioc.GetInstance<IRestaurantsResultsParser>();
            
            Console.WriteLine("Welcome to Just Eat's restaurant search.");

            var task = Run(apiClient, resultsParser, GetUserInput());

            Console.WriteLine("Thanks for using Just Eat's restaurant search.\nType any key to close the application:");
            Console.ReadKey();
            Environment.Exit(0);
        }

        private static string GetUserInput()
        {

            string input = "";
            while (input == "")
            {
                Console.Write("Enter your postcode or type exit to close the application:");
                input = Console.ReadLine();

                if (input == "exit")
                {
                    Environment.Exit(0);
                }
            }
            return input;
        }

        private static async Task Run(IApiClient apiClient, IRestaurantsResultsParser resultsParser, string postcode)
        {
            try
            {
                var restaurants = await apiClient.GetRestaurants(postcode).ConfigureAwait(false);
                var openRestaurants = restaurants.Restaurants.Where(r => r.IsOpenNow).ToList();

                var results = resultsParser.ParseResults(openRestaurants);
                Console.WriteLine(results);
            }
            catch (ClientException e)
            {
                OutputErrorAndExit(e);
            }
            catch (ServerException e)
            {
                OutputErrorAndExit(e);
            }
            catch (Exception e)
            {
                OutputErrorAndExit(new Exception("An unhandled exception occured.", e));
            }
        }

        private static void OutputErrorAndExit(Exception e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(e.Message);
            Console.ResetColor();
            Console.ReadKey();
            Environment.Exit(1);
        }
    }
}
