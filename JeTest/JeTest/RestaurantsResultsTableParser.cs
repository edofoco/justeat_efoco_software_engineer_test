﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JeTest.ApiClient.Models;

namespace JeTest
{
    public class RestaurantsResultsTableParser : IRestaurantsResultsParser
    {
        public string ParseResults(List<Restaurant> restaurants)
        {
            try
            {
                var stringBuilder = new StringBuilder();
                stringBuilder.AppendLine(string.Format("{3,-5}|{0, -50}|{1, -10}|{2,-50}|", "Name", "Rating", "Cuisines", ""));
                stringBuilder.AppendLine(string.Format("{3,-5}|{0,-50}|{1,-10}|{2,-50}|", Indent(50), Indent(10), Indent(50), Indent(5)));

                for (int i = 0; i < restaurants.Count; i++)
                {
                    var CuisineTypes = string.Join(",", restaurants[i].CuisineTypes.Select(t => t.Name));

                    stringBuilder.AppendLine(string.Format("{3,-5}|{0,-50}|{1,-10}|{2,-50}|", restaurants[i].Name, restaurants[i].Rating, CuisineTypes, i + 1));
                    stringBuilder.AppendLine(string.Format("{3,-5}|{0,-50}|{1,-10}|{2,-50}|", Indent(50), Indent(10), Indent(50), Indent(5)));
                }

                return stringBuilder.ToString();
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private static string Indent(int count)
        {
            return string.Empty.PadLeft(count, '─');
        }
    }
}
