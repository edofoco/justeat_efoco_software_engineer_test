﻿using JeTest.RestClient.AuthProvider;
using System.Net.Http;

namespace JeTest.RestClient.Models
{
    public interface IRequest
    {
        HttpRequestMessage HttpRequestMessage { get; set; }
        void AddHeader(string header, string value);
        void AddUrlParameter(string key, string value);
        void AddAuthorizationHeader(IAuthProvider authProvider);
    }
}
