﻿using JeTest.RestClient.Models;
using System.Net.Http;

namespace JeTest.RestClient
{
    public interface IRequestFactory
    {
        IRequest GetInstance(string baseUri, string resource, HttpMethod method);
    }
}
