﻿using JeTest.RestClient.Exceptions;
using System;
using System.Net.Http;
using JeTest.RestClient.AuthProvider;
using System.Net.Http.Headers;

namespace JeTest.RestClient.Models
{
    public class DefaultRequest : IRequest
    {
        public HttpRequestMessage HttpRequestMessage { get; set; }

        public DefaultRequest(string uri, string resource, HttpMethod method) {

            HttpRequestMessage = new HttpRequestMessage
            {
                RequestUri = new Uri($"{uri}/{resource}"),
                Method = method
            };
        }

        public void AddHeader(string header, string value)
        {
            try
            {
                switch (header.ToLower())
                {
                    case "content-type":
                        HttpRequestMessage.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(value));
                        break;
                    default:
                        HttpRequestMessage.Headers.Add(header, value);
                        break;
                }
            }
            catch (FormatException e) {
                throw new InvalidHeaderException($"{value} is not in a valid format for {header} header.");
            }
        }

        public void AddUrlParameter(string key, string value)
        {
            var newUrl = HttpRequestMessage.RequestUri.AbsoluteUri;

            if (!newUrl.Contains("?"))
            { 
                newUrl = $"{newUrl}?{key}={value}";
            }
            else {
                newUrl = $"{newUrl}&{key}={value}";
            }

            HttpRequestMessage.RequestUri = new Uri(newUrl);
        }

        public void AddAuthorizationHeader(IAuthProvider authProvider)
        {
            AddHeader("Authorization", authProvider.Token);
        }
    }
}
