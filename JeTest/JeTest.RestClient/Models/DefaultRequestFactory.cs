﻿using JeTest.RestClient.Models;
using System.Net.Http;

namespace JeTest.RestClient
{
    public class DefaultRequestFactory : IRequestFactory
    {
        public IRequest GetInstance(string baseUri, string resource, HttpMethod method)
        {
            return new DefaultRequest(baseUri, resource, method);
        }
    }
}
