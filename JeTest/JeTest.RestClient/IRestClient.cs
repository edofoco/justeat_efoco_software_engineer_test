﻿using JeTest.RestClient.AuthProvider;
using JeTest.RestClient.Models;
using System.Net.Http;
using System.Threading.Tasks;

namespace JeTest.RestClient
{
    public interface IRestClient
    {
        void AddDefaultHeader(string header, string value);
        Task<T> Execute<T>(IRequest request);
    }
}
