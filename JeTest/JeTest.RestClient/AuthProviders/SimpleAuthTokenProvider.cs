﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JeTest.RestClient.AuthProvider
{
    public class SimpleAuthTokenProvider : IAuthProvider
    {
        string _token;

        public SimpleAuthTokenProvider(string token)
        {
            _token = token;
        }

        public string Token
        {
            get
            {
                return _token;
            }
        }
    }
}
