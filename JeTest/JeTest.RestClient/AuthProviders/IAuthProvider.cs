﻿namespace JeTest.RestClient.AuthProvider
{
    public interface IAuthProvider
    {
        string Token { get; }
    }
}
