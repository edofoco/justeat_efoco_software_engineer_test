﻿using System;
using Newtonsoft.Json;

namespace JeTest.RestClient.Serializers
{
    public class JsonSerializer : ISerializer
    {
        public T Deserialize<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}
