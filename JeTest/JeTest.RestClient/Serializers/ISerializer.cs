﻿namespace JeTest.RestClient.Serializers
{
    public interface ISerializer
    {
        T Deserialize<T>(string data);
    }
}
