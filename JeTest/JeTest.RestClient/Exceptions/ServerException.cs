﻿using System;

namespace JeTest.RestClient.Exceptions
{
    public class ServerException: Exception
    {
        public ServerException(string message) : base(message) { }
    }
}
