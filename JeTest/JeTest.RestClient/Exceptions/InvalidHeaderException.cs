﻿using System;

namespace JeTest.RestClient.Exceptions
{
    public class InvalidHeaderException : Exception
    {
        public InvalidHeaderException(string message) : base(message) { }
    }
}
