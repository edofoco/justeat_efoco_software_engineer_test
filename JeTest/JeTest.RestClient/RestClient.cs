﻿using System.Net.Http;
using JeTest.RestClient.Models;
using JeTest.RestClient.Serializers;
using System.Threading.Tasks;
using JeTest.RestClient.Exceptions;
using System;
using System.Net.Http.Headers;

namespace JeTest.RestClient
{
    public class RestClient : IRestClient
    {
        private ISerializer _serializer;
        public HttpClient _httpClient;
        
        public RestClient(HttpClient httpClient, ISerializer serializer) {
            _serializer = serializer;
            _httpClient = httpClient;
        }
        
        public void AddDefaultHeader(string header, string value)
        {
            try
            {
                switch (header.ToLower()) {
                    case "content-type":
                        _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(value));
                        break;
                    default:
                        _httpClient.DefaultRequestHeaders.Add(header, value);
                        break;
                }
            }
            catch (FormatException e)
            {
                throw new InvalidHeaderException($"{value} is not in a valid format for {header} header.");
            }
        }

        public async Task<T> Execute<T>(IRequest request)
        {
            var response = _httpClient.SendAsync(request.HttpRequestMessage).Result;

            var data = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            if (!response.IsSuccessStatusCode)
            {
                HandleHttpException(response);
            }

            return _serializer.Deserialize<T>(data);
        }

        private void HandleHttpException(HttpResponseMessage response)
        {

            if ((int)response.StatusCode >= 400 && (int)response.StatusCode < 500)
            {
                throw new ClientException($"Get request failed due to invalid client request. {response}");
            }
            else if ((int)response.StatusCode >= 500 && (int)response.StatusCode < 600)
            {
                throw new ServerException($"Get request failed due to invalid server response. {response}");
            }
        }
        
    }
}
