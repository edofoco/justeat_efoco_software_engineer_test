﻿using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JeTest.ApiClient.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using JeTest.RestClient;
using JeTest.RestClient.Models;
using System.Net.Http;
using JeTest.RestClient.AuthProvider;

namespace JeTest.ApiClient.Tests
{
    [TestClass]
    public class ApiClientTests
    {
        private static Mock<IRestClient> _mockRestClient;
        private static Mock<IRequest> _mockRequest;
        private static Mock<IAuthProvider> _mockAuthProvider;
        private static Mock<IRequestFactory> _mockRequestFactory;

        [ClassInitialize]
        public static void Setup(TestContext tc)
        {
            _mockRestClient = new Mock<IRestClient>();
            _mockRequest = new Mock<IRequest>();
            _mockAuthProvider = new Mock<IAuthProvider>();
            _mockRequestFactory = new Mock<IRequestFactory>();
        }

        [TestMethod]
        public void Constructor_ValidHeaders_AssertHeadersAreAddedToRestClient()
        {
            var apiClient = new ApiClient("localhost.com", "uk", "je.apis.com", "en-GB", _mockRestClient.Object, _mockRequestFactory.Object, _mockAuthProvider.Object);
            
            _mockRestClient.Verify(m => m.AddDefaultHeader("Accept-Tenant", "uk"));
            _mockRestClient.Verify(m => m.AddDefaultHeader("Host", "je.apis.com"));
            _mockRestClient.Verify(m => m.AddDefaultHeader("Accept-Language", "en-GB"));
        }
        
        [TestMethod]
        public async Task GetRestaurants_WithPostcode_AssertAddUrlParametersAndAddAuthHeaderAndExecuteAreCalled()
        {
            _mockAuthProvider.Setup(m => m.Token).Returns("Basic VGVjaFRlc3RBUEk6dXNlcjI=");
            _mockRequest.Setup(m => m.AddUrlParameter("q", "w25na")).Verifiable();
            _mockRequestFactory.Setup(m => m.GetInstance(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<HttpMethod>())).Returns(_mockRequest.Object);
            
            var apiClient = new ApiClient("httpL//localhost", "uk", "je.apis.com", "en-GB", _mockRestClient.Object, _mockRequestFactory.Object, _mockAuthProvider.Object);
            
            await apiClient.GetRestaurants("w25na").ConfigureAwait(false);

            _mockRequest.Verify(m => m.AddUrlParameter("q", "w25na"));
            _mockRequest.Verify(m => m.AddAuthorizationHeader(It.IsAny<IAuthProvider>()));
            _mockRestClient.Verify(m => m.Execute<RestaurantsCollection>(It.IsAny<IRequest>()));
        }

        private RestaurantsCollection SampleRestaurants() {

            return new RestaurantsCollection
            {
                Restaurants = new List<Restaurant> {
                    new Restaurant {
                        Name = "You Me Sushi",
                        Rating = 2.9,
                        IsOpenNow = true,
                        CuisineTypes = new Cuisine[]{
                            new Cuisine {
                                Name = "Japanese"
                            },
                            new Cuisine {
                                Name = "Asian"
                            }
                        }
                    },

                     new Restaurant {
                        Name = "Mengal",
                        Rating = 4.0,
                        IsOpenNow = false,
                        CuisineTypes = new Cuisine[]{
                            new Cuisine {
                                Name = "Persian"
                            }
                        }
                     },

                     new Restaurant {
                        Name = "Star Of Bombay",
                        Rating = 5.0,
                        IsOpenNow = true,
                        CuisineTypes = new Cuisine[]{
                            new Cuisine {
                                Name = "Indian"
                            }
                        }
                     },

                     new Restaurant {
                        Name = "Pizza Express",
                        Rating = 3.0,
                        IsOpenNow = true,
                        CuisineTypes = new Cuisine[]{
                            new Cuisine {
                                Name = "Italian"
                            },
                            new Cuisine {
                                Name = "Pizza"
                            },

                        }
                     }
                }
            };
        }
    }
}
