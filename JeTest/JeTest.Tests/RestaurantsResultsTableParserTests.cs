﻿using JeTest.ApiClient.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace JeTest.Tests
{
    [TestClass]
    public class RestaurantsResultsTableParserTests
    {
        [TestMethod]
        public void ParseResults_ListOfRestaurants_AssertCorrectString()
        {
            var restaurantsParser = new RestaurantsResultsTableParser();
            var result = restaurantsParser.ParseResults(GetSampleRestaurants());

            Assert.AreEqual(ExpectedString(), result);
        }
        
        private List<Restaurant> GetSampleRestaurants()
        {
            return new List<Restaurant>{
                    new Restaurant {
                        Name = "You Me Sushi",
                        Rating = 2.9,
                        IsOpenNow = true,
                        CuisineTypes = new Cuisine[] {
                            new Cuisine {
                                Name = "Japanese",
                            },
                            new Cuisine {
                                Name = "Oriental"
                            }
                        }
                    },

                     new Restaurant {
                        Name = "Pizza Express",
                        Rating = 5.0,
                        IsOpenNow = false,
                        CuisineTypes = new Cuisine[] {
                            new Cuisine {
                                Name = "Italian",
                            },
                            new Cuisine {
                                Name = "Pizza"
                            }
                        }
                    },

                     new Restaurant {
                        Name = "Mengal",
                        Rating = 4.0,
                        IsOpenNow = true,
                        CuisineTypes = new Cuisine[] {
                            new Cuisine {
                                Name = "Persian",
                            }
                        }
                    },

                     new Restaurant {
                        Name = "The Diners",
                        Rating = 3.0,
                        IsOpenNow = false,
                        CuisineTypes = new Cuisine[] {
                            new Cuisine {
                                Name = "American",
                            }
                        }
                    }
            };
        }

        private string ExpectedString()
        {
            return @"     |Name                                              |Rating    |Cuisines                                          |
─────|──────────────────────────────────────────────────|──────────|──────────────────────────────────────────────────|
1    |You Me Sushi                                      |2.9       |Japanese,Oriental                                 |
─────|──────────────────────────────────────────────────|──────────|──────────────────────────────────────────────────|
2    |Pizza Express                                     |5         |Italian,Pizza                                     |
─────|──────────────────────────────────────────────────|──────────|──────────────────────────────────────────────────|
3    |Mengal                                            |4         |Persian                                           |
─────|──────────────────────────────────────────────────|──────────|──────────────────────────────────────────────────|
4    |The Diners                                        |3         |American                                          |
─────|──────────────────────────────────────────────────|──────────|──────────────────────────────────────────────────|
";
        }
    }
}
